@extends('layouts.mainlayout')
@section('style')
    <link href="{{asset('assets/custom_assets/jquery.orgchart.css')}}" rel="stylesheet">
    <style>
        .node{
            height: 70px !important;
        }
        .node h2{
            max-height: 15px;
            overflow-x: hidden;
        }
    </style>

@endsection
@section('content')
    @if(session()->has('message'))
        <div class="alert alert-success alert-dismissable" style="opacity: 0.75; width: 85%; margin-left: 10%" role="alert">
            {{ session()->get('message') }}
            <button class="close" data-dismiss="alert" aria-hidden="true">x</button>
        </div>
    @endif
    <div id="orgChart"></div>

    <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Append Member</h4>
                    <small class="text-right">Fields with <span style="color: red">*</span> signs are required.</small>
                </div>
                {!! Form::open(['url' => 'admin/users','method' => 'post', 'class' => 'form-horizontal usercreate_form']) !!}

                <div class="modal-body">
                    <div class="form-group {{ $errors->has('title1') ? 'has-error' : ''}}">
                        {!! Form::label('fname', "First Name", ['class' => 'col-md-2 control-label']) !!}

                        <div class="col-md-6">
                            {!! Form::text('fname', null, ['class' => 'form-control','id' => 'fname']) !!}
                            {!! $errors->first('fname', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>

                    </div>

                    <div class="form-group {{ $errors->has('title1') ? 'has-error' : ''}}">
                        {!! Form::label('lname', 'Last Name', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('lname', null, ['class' => 'form-control','id' => 'lname']) !!}
                            {!! $errors->first('lname', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                        {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('address', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                        {!! Form::label('contact', 'Contact', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('role','Role', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('role', ['user' => 'User', 'admin' => 'Admin'], 'user', ['class' => 'form-control','required']) !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>


                    <div class="form-group">
                        {!! Form::label('upline', 'Upline', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('upline', $user_child_low, '',['placeholder' => 'Select...', 'class' => 'form-control','id' =>'upline']) !!}
                            {!! $errors->first('upline', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('sponser', 'Sponser', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('sponser', $sponser, '', ['class' => 'form-control', 'id' => 'sponser']) !!}
                            {!! $errors->first('sponser', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('node_placement', 'Node placement', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('node_placement', [],'', ['placeholder' => 'Select Upline first', 'class' => 'form-control','id' => 'node']) !!}
                            {!! $errors->first('node_placement', '<p class="help-block">:message</p>') !!}
                        </div>

                        <span style='color: red; font-size: 13pt'>*</span>
                        <div id="node_loading">

                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                        {!! Form::label('username', 'Username', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('username', null, ['class' => 'form-control','id' => 'username']) !!}
                            {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                        {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email']) !!}
                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('password', ['class' => 'form-control','id' => 'password']) !!}
                            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div class="form-group">
                        {!! Form::label('cpassword', 'Confirm Password', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::password('cpassword', ['class' => 'form-control', 'id' => 'cpassword']) !!}
                        </div>
                        <span style='color: red; font-size: 13pt'>*</span>
                    </div>
                    <div id="error-box"><ul></ul></div>
                </div>
                <div class="modal-footer">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-success" id="btn-submit"><i class="fa fa-check"></i> Create</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Cancel</button>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@section('script')
    <script src="{{asset('assets/custom_assets/jquery.orgchart.js')}}"></script>
    <script>

        var testData = [
            {id: '{{Auth::user()->id}}', actualid: '{{Auth::user()->id}}',name: '{{Auth::user()->fname." ".Auth::user()->lname}}',lbv:'{{Auth::user()->lbv}}', rbv:'{{Auth::user()->rbv}}',parent: 0},
            @if($users != null)
                @foreach($users as $user)
            {id: '{{$user->temp_id}}', actualid: '{{$user->user_id}}', name: '{{$user->user->fname." ".$user->user->lname}}',lbv: '{{$user->user->lbv}}', rbv: '{{$user->user->rbv}}',parent: parseInt('{{$user->upline}}')},
            @endforeach
            @endif
        ];
        console.log(testData);
        $(function(){
            org_chart = $('#orgChart').orgChart({

                data: testData,// your data
                depth: 0,
                newNodeText: 'Add Child',// text of add button
                showControls:true,// display add or remove node button.=

                onAddNode: function(node){
                    console.log(node.data)
                    $('#modal-default').modal('show');
                    $('#sponser').val(node.data.actualid);

                    console.log('Created new node on node '+node.data.id);
                    //make new node
                    // org_chart.newNode(node.data.id);
                },
                onDeleteNode: function(node){
                    console.log('Deleted node '+node.data.id);
                    org_chart.deleteNode(node.data.id);
                },
                onClickNode: function(node){
                    console.log('Clicked node '+node.data.actualid);
                },



        });
        });
        $('.usercreate_form').on('submit', function () {
            if($('#password').val() !== $('#cpassword').val()){
                alert('Passwords donot match!');
                return false;
            }
        });
        $('#upline').change(function () {
            $('#node_loading').append(`<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>`);
            let id = $(this).val();
            $.ajax({
                url: '{{url('/user/checknodeplacement')}}',
                type: 'get',
                data: {
                    id : id,
                },
                success: function (data) {
                    console.log(data);
                    if(data[0] === 'left' && data[1] === 'right'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 0,
                            text: 'Left'
                        }));
                        $('#node').append($('<option>', {
                            value: 1,
                            text: 'Right'
                        }));
                    }else if(data[0] === 'left'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 0,
                            text: 'Left'
                        }));
                    }else if(data[1] === 'right'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 1,
                            text: 'Right'
                        }));
                    }
                    $('#node_loading').empty();
                }
            })
        });
        $('#btn-submit').on('click',function (e) {
            e.preventDefault();
            if($('#fname').val() == ""){
                alert('First name is mandatory');
                return false;
            }
            if($('#lname').val() == ""){
                alert('Last name is mandatory');
                return false;
            }
            if($('#upline').val() == ""){
                alert('Select parent node');
                return false;
            }
            if($('#node').val() == ""){
                alert('Select Node Placement');
                return false;
            }
            if($('#username').val() == ""){
                alert('Username Required!');
                return false;
            }
            if($('#password').val() == "" || $('#cpassword').val() == ""){
                alert('Password field shouldn\'t be empty!');
                return false;
            }
            if($('#password').val() !== $('#cpassword').val()){
                alert('Passwords donot match!');
                return false;
            }
            let form = $(this).parents('form:first');
            let formarray = form.serializeArray();

            if(formarray)
            $.ajax({
                url: '{{url('admin/users')}}',
                type: 'post',
                data: formarray,
                success: function (data) {
                    $('.modal-body').html(`<div class="alert alert-success" style="opacity: .75"><h1>Success!</h1> <br><h3>Now please wait for the page to reload...or if it doesnt, reload the page manually.</h3></div>`)
                    location.reload();
                },
                error: function (data) {
                    let response = $.parseJSON(data.responseText);
                    let errors = response.errors;
                    $("#error-box").addClass('alert alert-danger');
                    $.each(errors, function (key, value) {
                        $("#error-box").find('ul').append(`<li>`+value+`</li>`);
                    });
                }
            });
        });


    </script>
@endsection