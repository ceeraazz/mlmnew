@extends('layouts.mainlayout')
@section('style')
    <style>
        .glyphicon-refresh-animate {
            -animation: spin .7s infinite linear;
            -webkit-animation: spin2 .7s infinite linear;
        }

        @-webkit-keyframes spin2 {
            from { -webkit-transform: rotate(0deg);}
            to { -webkit-transform: rotate(360deg);}
        }

        @keyframes spin {
            from { transform: scale(1) rotate(0deg);}
            to { transform: scale(1) rotate(360deg);}
        }
    </style>
@endsection
@section('content')
    <div class="box box-success">

        <div class="box-body">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissable" style="opacity: 0.75" role="alert">
                    {{ session()->get('message') }}
                    <button class="close" data-dismiss="alert" aria-hidden="true">x</button>
                </div>
            @endif
            {!! Form::model($user, ['url' => 'admin/users/'.$user->id,'method' => 'put', 'class' => 'form-horizontal useredit_form']) !!}
                <div class="form-group {{ $errors->has('fname') ? 'has-error' : ''}}">
                    {!! Form::label('fname', 'First Name', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('fname', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('fname', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>

                <div class="form-group {{ $errors->has('lname') ? 'has-error' : ''}}">
                    {!! Form::label('lname', 'Last Name', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('lname', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('lname', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
                    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('address', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('contact') ? 'has-error' : ''}}">
                    {!! Form::label('contact', 'Contact', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::text('contact', null, ['class' => 'form-control']) !!}
                        {!! $errors->first('contact', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('role','Role', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('role', ['user' => 'User', 'admin' => 'Admin'], null, ['class' => 'form-control','required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('upline', 'Upline', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('upline', ($user->user_detail)?[$user->user_detail->upline => $user->fname]:[], '',['class' => 'form-control','id' =>'upline']) !!}
                        {!! $errors->first('upline', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group">
                    {!! Form::label('sponser', 'Sponser', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('sponser', $sponser, ($user->user_detail)?$user->user_detail->sponser:null, ['placeholder' => 'Select if any', 'class' => 'form-control']) !!}
                        {!! $errors->first('sponser', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group">
                {!! Form::label('node_placement', 'Node placement', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::select('node_placement', $node, null, ['class' => 'form-control','id' => 'node']) !!}
                    {!! $errors->first('node_placement', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="col-md-1" id="node_loading">

                </div>
            </div>
            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                {!! Form::label('username', 'Username', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::text('username', null, ['class' => 'form-control','id' => 'username']) !!}
                    {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
                <div class="form-group {{session()->has('old_password') ? 'has-error' : ''}}">
                    {!! Form::label('old_password', 'Current Admin Password', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::password('old_password', ['class' => 'form-control','id' => 'old_password']) !!}
                        {!! $errors->first('old_password', '<p class="help-block">:message</p>') !!}
                        @if(session()->has('old_password'))
                            <p class="help-block">{{ session()->get('old_password') }}</p>
                        @endif
                    </div>
                </div>
            <div class="form-group {{session()->has('new_password')?'has-error':''}} {{ $errors->has('new_password') ? 'has-error' : ''}}">
                {!! Form::label('new_password', 'New Password', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('new_password', ['class' => 'form-control','id' => 'password']) !!}
                    {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
                    @if(session()->has('new_password'))
                        <p class="help-block">{{ session()->get('new_password') }}</p>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('new_password_confirmation', 'Confirm Password', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'id' => 'new_password_confirmation']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>Save</button>
                    <a href="{{ url('/admin/users/'.$user->id.'/edit') }}" class="btn btn-default"><i class="fa fa-reply" aria-hidden="true"></i> Reset old values</a>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            // for ajax
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
        $('.usercreate_form').on('submit', function () {
            if($('#password').val() !== $('#cpassword').val()){
                alert('Passwords donot match!');
                return false;
            }
        });
        $('#upline').change(function () {
            $('#node_loading').append(`<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>`);
            let id = $(this).val();
            $.ajax({
                url: '{{url('/user/checknodeplacement')}}',
                type: 'get',
                data: {
                    id : id,
                },
                success: function (data) {
                    console.log(data);
                    if(data[0] === 'left' && data[1] === 'right'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 0,
                            text: 'Left'
                        }));
                        $('#node').append($('<option>', {
                            value: 1,
                            text: 'Right'
                        }));
                    }else if(data[0] === 'left'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 0,
                            text: 'Left'
                        }));
                    }else if(data[1] === 'right'){
                        $('#node').empty();
                        $('#node').append($('<option>', {
                            value: 1,
                            text: 'Right'
                        }));
                    }
                    $('#node_loading').empty();
                }
            })
        });
    </script>
@endsection