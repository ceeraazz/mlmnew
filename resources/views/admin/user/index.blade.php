@extends('layouts.mainlayout')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users List</h3>

                    <div class="box-tools">
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        @if(count($users))
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>Contact</th>
                                <th>Sponser's Name</th>
                                <th>Senior's Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>

                            @foreach($users as $index => $user)
                                <tr>
                                    <td>{{$index + $users->firstItem()}}</td>
                                    <td>{{$user->fname." ".$user->lname}}</td>
                                    <td>{{$user->address}}</td>
                                    <td>{{$user->contact}}</td>
                                    <td>@if($user->user_detail->sponser){{$user->getfname($user->user_detail->sponser)}}@endif</td>
                                    <td>@if($user->user_detail->upline){{$user->getfname($user->user_detail->upline)}}@endif</td>
                                    <td>{{$user->username}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ url('/admin/users/' . $user->id . '/edit') }}" title="Edit User"><button class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button></a>

                                            {!! Form::open([
                                                        'method'=>'DELETE',
                                                        'url' => ['admin/users', $user->id],
                                                        'style' => 'display:none'
                                                    ]) !!}

                                                    {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User" />', [
                                                            'type' => 'submit',
                                                            'class' => '',
                                                    ]);!!}
                                                    {!! Form::close() !!}
                                            @if($user->id != '1')
                                                    <a href="#"><button class="btn btn-danger delete_btn" data-id="{{$user->id}}"><span class="glyphicon glyphicon-trash" aria-hidden="true" title="Delete User"></span> Delete</button></a>
                                            @endif
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            {{$users->links()}}
                        @else
                            <div class="box-body">
                                <div class="callout bg-gray">
                                    <h4>Empty</h4>
                                    <p>No Users Added</p>
                                </div>
                            </div>
                        @endif
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('.delete_btn').on('click', function () {
            $.ajax({
                url: '{{url('/user/has-children')}}',
                type: 'get',
                data: {
                    id : $(this).data('id'),
                },
                success: function (data) {
                    if(parseInt(data) > 0){
                        if(confirm('Confirm delete? Please be informed that some children exist for this entry. All children will be deleted too.'))
                            $(this).parent().find('form').submit();
                        else
                            return false;
                    } else {
                        if(confirm('Confirm delete?'))
                            $(this).parent().find('form').submit();
                        else
                            return false;
                    }
                }
            });

        })
    </script>
@endsection