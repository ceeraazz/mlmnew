@extends('layouts.mainlayout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3>Transfer/Manage Funds</h3>


                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="box-tools">
                        <p class="help-block error-msg text-center" style="color: red"></p>
                        {!! Form::open(['url' => 'balance-transfer','method' => 'post', 'class' => 'form-horizontal', 'id' =>'balanceform']) !!}
                        <table class="table table-borderless">
                            <tr>
                                <td style="text-align: right; border-top: solid darkgrey;border-bottom: none" >
                                    {!! Form::label('from', 'From', ['class' => 'control-label']) !!}
                                </td>
                                <td style="border-top: solid darkgrey;border-bottom: none">
                                    {!! Form::select('from', $users, '', ['placeholder' => 'Select Sender', 'class' => 'form-control', 'id' => 'from']) !!}
                                    {!! $errors->first('from', '<p class="help-block">:message</p>') !!}
                                </td>
                                <td style="text-align: right; border-left: solid darkgrey; border-top:none;border-bottom: none">
                                    {!! Form::label('to', 'To', ['class' => 'control-label']) !!}
                                </td>
                                <td style="border-top:none;border-bottom: none">
                                    {!! Form::select('to', $users, '', ['placeholder' => 'Select Receiver', 'class' => 'form-control', 'id' => 'to']) !!}
                                    {!! $errors->first('to', '<p class="help-block">:message</p>') !!}
                                </td>



                            </tr>
                            <tr>
                                <td style="text-align: right; border-top:none;border-bottom: none">
                                    {!! Form::label('minus_amt', 'Amount', ['class' => 'control-label']) !!}
                                </td>
                                <td style="border-top:none;border-bottom: none">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <span class="fa fa-minus"></span>
                                            </div>
                                            {!! Form::input('number','minus_amt', 0, ['placeholder' => 'Deductant Amount', 'class' => 'form-control', 'id' => 'minus_amt']) !!}
                                        </div>
                                        {!! $errors->first('minus_amt', '<p class="help-block">:message</p>') !!}

                                </td>
                                <td style="text-align: right; border-left: solid darkgrey; border-top:none;border-bottom:  solid darkgrey">
                                    {!! Form::label('plus_amt', 'Amount', ['class' => 'control-label']) !!}
                                </td>
                                <td style="border-top:none;border-bottom: solid darkgrey">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="fa fa-plus"></span>
                                        </div>
                                        {!! Form::input('number','plus_amt', 0, ['placeholder' => 'Add Amount', 'class' => 'form-control', 'id' => 'plus_amt']) !!}
                                    </div>
                                    {!! $errors->first('plus_amt', '<p class="help-block">:message</p>') !!}
                                </td>

                            </tr>
                            <tr>
                                <td style="border-top:none"></td>
                                <td style="border-top:none">
                                    {!! Form::submit('Transfer', ['class' => 'btn btn-primary submit-btn', 'onsubmit' => "return ConfirmDelete()"]) !!}
                                </td>
                                <td style="border-top:none"></td>
                                <td style="border-top:none"></td>
                            </tr>
                        </table>
                        {!! Form::close() !!}
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
@endsection
@section('script')
    <script>
        $('#balanceform').submit(function () {
            let from = $('#from').val();
            let to = $('#to').val();
            if(from === '' && to === ''){
                $('.error-msg').html('*Both fields cannot be empty.');
                return false;
            }
            if(from === to){
                $('.error-msg').html('*Both fields cannot be same.');
                return false;
            }
        });
    </script>
@endsection