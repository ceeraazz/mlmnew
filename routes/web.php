<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Auth::routes();
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


//Admin routes
Route::get('/', 'UserController@dashboard');
Route::resource('/admin/users', 'UserController');
Route::resource('/treeview', 'TreeViewController');
Route::resource('/balance-transfer', 'BalanceController');

//ajax routes in admin
Route::get('/user/checknodeplacement', 'UserController@check_node_placement');
Route::post('/user/quick-add-user', 'TreeViewController@quick_add_user');
Route::get('/user/has-children', 'UserController@has_children');
