<?php

namespace App\Http\Controllers;

use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TreeViewController extends Controller
{

    protected $array_child= [];


    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $users = $this->recursive_child(Auth::user());

        $sponser = User::pluck('fname', 'id');

        //for generating upline field in quick add user form
        $user_child_low = [];
        $user_all = User::all();
        foreach($user_all as $user_each) {
            $children = UserDetail::where('upline', $user_each->id)->get();
            if(count($children) < 2){
                $user_child_low[$user_each->id] = $user_each->fname;
            }
        }
        return view('treeview', compact('users', 'sponser', 'user_child_low'));
    }

    function recursive_list($users) {
        echo '<ul>';
        foreach($users as $user){
            echo '<li><a href="#">' . $user['fname'] . '</a>';
            if(array_key_exists('children', $user)) {
                $this->recursive_list($user);
            }
        }
    }



    function recursive_child($user){
        $children = UserDetail::where('upline', $user->id)->get();
        if(count($children) > 0){
            if(count($children) == 2){
                if($children[0]['node_placement'] == '1'){

                    $children[0]['temp_id'] = $children[1]['user_id'];
                    $children[1]['temp_id'] = $children[0]['user_id'];

                } else {
                    $children[0]['temp_id'] = $children[0]['user_id'];
                    $children[1]['temp_id'] = $children[1]['user_id'];
                }
            } else {
                $children[0]['temp_id'] = $children[0]['user_id'];
            }
            foreach($children as $child){
                $this->array_child[] = $child;
                $this->recursive_child(User::find($child->user_id));

            }

        } else {
            return false;
        }
        return $this->array_child;

    }

//    public function quick_add_user(Request $request){
//
//        $validatedData = $request->validate([
//            'username' => 'required|unique:users|max:255'
//        ]);
//        $user = $request->only('fname', 'lname', 'role', 'username', 'email', 'password');
//        $user['password'] = bcrypt($user['password']);
//        $new_user = User::create($user);
//        if($request->has('node_placement')){
//            $user_detail = $request->except('fname', 'lname', 'role', 'username', 'email', 'password');
//            UserDetail::create([
//                'user_id' => $new_user->id,
//                'upline' => $user_detail['upline'],
//                'sponser' => $user_detail['sponser'],
//                'node_placement' => $user_detail['node_placement']
//
//            ]);
//        }
//        Session::flash('flash_message', 'User Added!');
//        return back();
//    }
}
