<?php

namespace App\Http\Controllers;

use App\Earning;
use App\User;
use Illuminate\Http\Request;

class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::pluck('fname', 'id');

        return view('admin.balance', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $from = $request->from;
        $to = $request->to;
        try{
            if($from != null){
                $user = Earning::findOrFail($from);
                $user->amount = $user->amount - $request->minus_amt;
                $user->update();
            }
            if($to != null){
                $user = Earning::findOrFail($to);
                $user->amount = $user->amount + $request->plus_amt;
                $user->update();
            }
            $message = array(
                'message' => "Success!",
                'alert-type' => 'success'
            );
        } catch (\Exception $e){
            $message = array(
                'message' => "Error! Please restart the page and try again.",
                'alert-type' => 'error'
            );
        }
        return back()->with($message);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
