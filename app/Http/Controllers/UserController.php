<?php

namespace App\Http\Controllers;

use App\Earning;
use App\User;
use App\UserDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::where('id', '!=', Auth::user()->id)->paginate(10);

        return view('admin.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_collection = [];
        $users = User::all();
        foreach($users as $user) {
            $children = UserDetail::where('upline', $user->id)->get();
            if(count($children) < 2){
                $user_collection[$user->id] = $user->fname;
            }
        }
        $sponser = User::pluck('fname', 'id');
        return view('admin.user.create', compact('user_collection', 'sponser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'fname' => 'required|max:255',
            'lname' => 'required|max:255',
            'username' => 'required|unique:users|max:255',
//            'email' => 'unique:users',
            'password' => 'required|string|min:8',
        ])->validate();


        $user = $request->only('fname', 'lname', 'role','address','contact', 'username', 'email', 'password');
        $user['password'] = bcrypt($user['password']);
        $new_user = User::create($user);
        if($request->has('node_placement')){
            $user_detail = $request->except('fname', 'lname', 'role','address','contact', 'username', 'email', 'password');

            UserDetail::create([
                'user_id' => $new_user->id,
                'upline' => $user_detail['upline'],
                'sponser' => $user_detail['sponser'],
                'node_placement' => $user_detail['node_placement']

            ]);
            $this->add_bookvalue($request['upline'],$request['node_placement']);
        }
        Earning::create([
            'user_id' => $new_user->id,
            'amount' => 0,
            'level' => ''
            ]);
        return back()->with('message', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $sponser = User::pluck('fname', 'id');
        $user_collection = [];
        $users = User::all();
        foreach($users as $userr) {
            $children = UserDetail::where('upline', $userr->id)->get();
            if(count($children) < 2){
                $user_collection[$userr->id] = $userr->fname;
            }
        }
        if($user->user_detail){
        switch ($user->user_detail->node_placement) {
            case 0:
                $node = [0 => "Left"];
                break;
            case 1:
                $node = [1 => "Right"];
                break;
            default:
                $node = [];
        }
        } else {
            $node = [];
        }

        return view('admin.user.edit', compact('user', 'sponser','node', 'user_collection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $hashedPassword = Auth::user()->password;
        if(\Hash::check($request->old_password, $hashedPassword)) {
            if (!\Hash::check($request->new_password, $hashedPassword)){
                Validator::make($request->all(), [
                    'fname' => 'required|max:255',
                    'lname' => 'required|max:255',
                    'username' => 'unique:users,username,'.$id.'|max:255|required',
//                    'email' => 'unique:users,email,'.$id,
                    'new_password' => 'confirmed'
                ])->validate();

                $user = $request->only('fname', 'lname','address','contact', 'role', 'username', 'email', 'new_password');
                $user['new_password'] = bcrypt($user['new_password']);
                $updated_user = User::findOrFail($id)->fill($user)->save();
                session()->flash('message','Updated Successfully!');
            } else{
                session()->flash('new_password','new password can not be the old password!');
            }
        } else {
            session()->flash('old_password','old password doesn\'t match ');
            return redirect()->back();
        }
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $user = User::findorFail($id);
            $user->user_detail()->delete();
            $user->delete();

        }catch (\Exception $e) {

        }
        return back();
    }

    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function check_node_placement($id = null) {
        $id = $_REQUEST['id'];
        $nodes = [];
        $check_user_childs = UserDetail::where('upline', $id)->first();
        if ($check_user_childs == []) {
            $nodes[0] = 'left';
            $nodes[1] = 'right';
        } elseif ($check_user_childs->node_placement == '0') {
            $nodes[1] = 'right';
        }
        elseif ($check_user_childs->node_placement == '1'){
            $nodes[0] = 'left';
        }
        return $nodes;
    }



    public function has_children(){
        $children = UserDetail::where('upline', $_REQUEST['id'])->count();
        return $children;
    }

    public function add_bookvalue($upline, $node_placement){
        if($upline != ''){
            $parent = User::findOrFail($upline);
            if($node_placement == '0')
                $parent['lbv'] = $parent['lbv'] + 60;
            else
                $parent['rbv'] = $parent['rbv'] + 60;
            $parent->update();
            $userdetail = UserDetail::where('user_id',$parent->id)->first();
            if(isset($userdetail->upline))
            $this->add_bookvalue($userdetail->upline, $userdetail->node_placement);
            else
                $this->add_bookvalue('', '');
        } else {
            return true;
        }
    }
}
