<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
        'user_id', 'upline', 'sponser', 'node_placement'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

}


